# CoTea
[![](https://jitpack.io/v/com.gitlab.umbrellait-mikhail-eremkin/cotea.svg?style=flat-square)](https://jitpack.io/#com.gitlab.umbrellait-mikhail-eremkin/cotea)

__CoTea (Coroutine The Elm Architecture)__ - библиотека для создания приложений Android с использованием Elm-архитектуры, в которой для асинхронного управления потоком данных используются Kotlin корутины. 

#### Подключение библиотеки
```
dependencies {
  implementation ("com.gitlab.umbrellait:cotea:$version")
}
```
#### Автоматическая генерация CoTea классов
Для для каждого экрана требуется создать набор классов и интерфейсов (State, Message, SideEffect, Command, CommandHandler, StateUpdater, StoreFactory, Analytic, ViewModel). 
[Plugin для AS/IDEA](https://gitlab.com/umbrella-web/mobilesandbox/android-department-documentation/tools/android-idea-plugin) позволяет упростить это процесс и создавать классы автоматически. 
Как это сделать можно посмотреть в [инструкции](https://gitlab.com/umbrella-web/mobilesandbox/android-department-documentation/tools/android-idea-plugin/-/tree/main/docs/plugin_features/cotea_files_generation). 

#### Выпуск новой версии
Чтобы выпустить новую версию необходимо выполнить следующие действия
1. Запустить скрипт ./deploy.sh {version}, где {version} - номер версии, которую нужно выпустить.<br> 
Например, `./deploy.sh 0.11`
2. Перейти на сайт [https://jitpack.io](https://jitpack.io/#com.gitlab.umbrellait-mikhail-eremkin/cotea)
3. Найти в списке версию, указанную в п.1, и если кнопка "Get it" серого цвета - нажать на нее, чтобы запустить pipeline сборки версии
4. Дождаться окончания сборки, кнопка "Get it" должна поменять цвет на зеленый.

#### Полезные ссылки

1. [MVI на андроид, Hannes Dorfmann](https://hannesdorfmann.com/android/mosby3-mvi-1/)
2. [Разбираем ELM архитектуру в рамках мобильного приложения (Habr)](https://habr.com/ru/companies/vivid_money/articles/550932/)
---
__(c) UmbrellaIT, Mobile department, 2024__