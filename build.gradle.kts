// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version libs.versions.androidPlugin apply false
    id("com.android.library") version libs.versions.androidPlugin apply false
    kotlin("jvm") version libs.versions.kotlin apply false
    kotlin("android") version libs.versions.kotlin apply false
}